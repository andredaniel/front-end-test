import Vue from 'vue'
import { sync } from 'vuex-router-sync'
import App from './components/App'
import router from './router'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'

sync(store, router)

const app = new Vue({
  router,
  store,
  ...App
})

Vue.use(VueAxios, axios)
// Vue.use(require('vue-resource'))

require('font-awesome/css/font-awesome.css')

export { app, router, store }
